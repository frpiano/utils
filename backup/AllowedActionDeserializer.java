//package deserializer;
//
//import java.io.IOException;
//
//import com.fasterxml.jackson.core.JsonParser;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.DeserializationContext;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
//
//import domain.AllowedAction;

/* deserializzatore Java per l'enum AllowedAction, anch'essa fatta in java ma che aveva problemi con gli attori
 * anche se funzionante in un main()
 */

//public class AllowedActionDeserializer extends StdDeserializer<AllowedAction> {
//
//	private static final long serialVersionUID = 5724048999592566014L;
//	
//	public AllowedActionDeserializer() {
//		this(null);
//	}
//	
//	public AllowedActionDeserializer(Class<?> vc) { 
//        super(vc); 
//    }
//
//	@Override
//	public AllowedAction deserialize(JsonParser jp, DeserializationContext ctxt)
//			throws IOException, JsonProcessingException {
//
//		JsonNode node = jp.getCodec().readTree(jp);
//		int id = node.asInt();
//
//		AllowedAction toReturn = null;
//		for (AllowedAction ac : AllowedAction.values()) {
//			if (ac.getId() == id) {
//				toReturn = ac;
//			}
//		}
//
//		if (toReturn == null) {
//			throw new IllegalStateException("Cannot deserialize AllowedAction");
//		}
//
//		return toReturn;
//	}
//
//}
