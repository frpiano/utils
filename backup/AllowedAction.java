//package domain;
//
//import com.fasterxml.jackson.annotation.JsonValue;
//import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
//
//import deserializer.AllowedActionDeserializer;
////@JsonFormat(shape = JsonFormat.Shape.OBJECT)
//@JsonDeserialize(using = AllowedActionDeserializer.class)
//public enum AllowedAction {
//	READY_FOR_ORDER(1, "Ready for order"),
//	ORDER(2, "Order"),
//	CONSUME_FINISHED(3, "Consume Finished"),
//	PAY(4, "Pay"),
//	NONE(5, "Nothing");
//	
//	private int id;
//	private String value;
//	
//	@JsonValue
//	public int getId() {
//		return id;
//	}
//
//	public String getValue() {
//		return value;
//	}
//
//	
//	AllowedAction(int id, String value) {
//		this.id = id;
//		this.value = value;
//	}
//
//}
