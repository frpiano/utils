package util


import com.fasterxml.jackson.module.kotlin.*
import com.fasterxml.jackson.annotation.JsonValue

object JSONUtils {

	private val mapper = jacksonObjectMapper()

	fun toJSON(obj: Any): String {

		return this.toJSON(obj, false)

	}

	fun toJSON(obj: Any, withPretty: Boolean): String {

		if (withPretty) {
			return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
		} else {
			return mapper.writeValueAsString(obj);
		}
	}
}