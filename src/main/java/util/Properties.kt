package util

import java.io.FileOutputStream
import java.io.FileInputStream
import java.io.IOException
import java.util.Properties
import java.text.MessageFormat

class AppProperties private constructor() {

	val properties: Properties
	val inputStream : FileInputStream

	init {
		properties = Properties()
		val propertiesFile = System.getProperty("user.dir") + "/app.properties";

		inputStream = FileInputStream(propertiesFile)
	}

	private object GetInstance {
		val INSTANCE = AppProperties()
	}
	
	companion object {
		val instance: AppProperties by lazy { GetInstance.INSTANCE }
	}
	
	fun getProperty(property : String, vararg args : Any) : String{
		return MessageFormat.format(properties.getProperty(property), args)
	}
	
	fun getProperty(property : String) : String {
		return properties.getProperty(property)
	}
}