package util

import java.io.File

object WalkerUtils {


	fun checkMapExist(mapName: String): Boolean {
		
		var fileName = mapName + ".bin";

		var file = File(fileName)

		if (file.exists()) {
			return true
		} else {
			return false
		}
	}

	/*	If it is a table ir returns 1 or 0 (table index) otherwise 0 if it is a wall	*/
	fun checkCollisionWithTable(currentDirection: String, currentX: Int, currentY: Int): Int {
		if (currentDirection == "UP") {
			if (currentY == 4) {
				if (currentX >= 1 && currentX <= 3) {
					return 1
				}
				if (currentY > 3 && currentY <= 5) {
					return 2
				}
			}
		} else if (currentDirection == "DOWN") {
			if (currentY == 2) {
				if (currentX >= 1 && currentX <= 3) {
					return 1
				}
				if (currentY > 3 && currentY <= 5) {
					return 2
				}
			}
		} else if (currentDirection == "LEFT") {
			if (currentY >= 2 && currentY <= 4) {
				if (currentX == 3) {
					return 1
				}
				if (currentX == 5) {
					return 2
				}
			}
		} else {    /*if( currentDirection == "RIGHT" ){*/
			if (currentY >= 2 && currentY <= 4) {
				if (currentX == 1) {
					return 1
				}
				if (currentX == 3) {
					return 2
				}
			}
		}
		return 0
	}

	/* decide la sequenza di mosse dopo un fail*/
	fun getMoveAfterFail(currentDirection: String, currentX: Int, currentY: Int, targetX: Int, targetY: Int): Boolean {
		if (currentDirection == "UP") {
			if (currentX < targetX) {
				return false
			} else {
				return true
			}
		} else if (currentDirection == "DOWN") {
			if (currentX >= targetX) {
				return true
			} else {
				return false
			}
		} else if (currentDirection == "LEFT") {
			if (currentY < targetY) {
				return false
			} else {
				if (currentY > targetY) {
					return true
				} else {
					if (currentY >= 4) {
						return false
					} else {
						return true
					}
				}
			}
		} else {         //if( currentDirection == "RIGHT" ){
			if (currentY > targetY) {
				return false
			} else {
				if (currentY < targetY) {
					return true
				} else {
					if (currentY >= 4) {
						return true
					} else {
						return false
					}
				}
			}
		}
	}
}