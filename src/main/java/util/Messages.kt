package util

import java.text.MessageFormat
import java.util.Locale;
import java.util.ResourceBundle;
import java.io.File
import java.net.URLClassLoader
import java.net.URL

//https://grokonez.com/kotlin/kotlin-properties-read-write-properties-file-properties-xml-file#I_Kotlin_8211_ReadWrite_Properties_fromto_Properties_file
//https://stackoverflow.com/questions/56663736/how-to-set-placeholder-values-in-properties-file-in-spring
//https://mkyong.com/java/java-resourcebundle-example/
// Gestione dei messaggi tramite risorse esterne
class Messages private constructor(){

	val resourceBundle : ResourceBundle;
	val file : File
	val urls : Array<URL>
	
	init {
		file = File(".");
        urls = arrayOf(file.toURI().toURL());
		
        val loader = URLClassLoader(urls)
        Locale.setDefault(Locale("en", "US"));
		resourceBundle = ResourceBundle.getBundle("messages", Locale.getDefault(), loader);
	}
	
	private object GetInstance {
        val INSTANCE = Messages()
    }
	
	companion object {
        val instance: Messages by lazy { GetInstance.INSTANCE }
    }
	
	fun getMessage(message : String) : String {
		return resourceBundle.getString(message)
	}
	
	fun getMessage(message : String, vararg args : Any) : String {
		return  MessageFormat.format(resourceBundle.getString(message), *args)
	}

}

fun main(args: Array<String>) {
	
	var messages = Messages.instance
	println(messages.getMessage("hello.world"))
	println(messages.getMessage("moving.table", 1));

}