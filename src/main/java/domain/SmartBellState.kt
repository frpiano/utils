package domain

// Mappa degli stati dello Smartbell
enum class SmartBellState {
	// attendo un cliente
	WAITING_FOR_CLIENT,
	// controllo temperatura cliente
	CHECK_TEMPERATURE,
	// nega ingresso cliente
	DENY_ENTRANCE,
	// richiesta di un tavolo
	TABLE_REQUEST,
	// fai entrare un cliente
	ENTER_CLIENT,
	// attendi di entrare
	WAIT_FOR_TIME,
	// aggiorna il tempo di attesa
	UPDATE_WAIT_TIME,
	// il cliente e' andato via
	CLIENT_GONE,
	UNKNOWN
}