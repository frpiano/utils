package domain

import com.fasterxml.jackson.module.kotlin.*
import com.fasterxml.jackson.annotation.JsonValue

enum class OrderState(val id: Int, @get:JsonValue val value: String) {
	SUBMIT(1, "Submit"),
	PREPARE(2, "Prepare"),
	READY(3, "Ready"),
	COMPLETE(4, "Completed")
}