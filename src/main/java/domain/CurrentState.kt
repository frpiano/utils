package domain

import java.util.*
import com.fasterxml.jackson.module.kotlin.*
import java.lang.IllegalStateException
import com.fasterxml.jackson.annotation.JsonValue
import domain.TableState
import domain.OrderState
import domain.WaiterState
import domain.Product
import domain.AllowedAction
import domain.CheckTemperature
import util.Messages
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.databind.DeserializationFeature

//https://bezkoder.com/kotlin-convert-json-to-object-jackson/
@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrentState private constructor() {
	val tableMap = hashMapOf<Int, TableState>()
	val orderMap = hashMapOf<Int, OrderState>()
	var targetTable: Int
	var targetOrder: Int
	var waiterState: WaiterState
	var waiterPosX: Int
	var waiterPosY: Int
	var description: String
	var waitingClient: Boolean
	var waitingClientAtExit: Boolean
	var clientFound: Boolean
	var minTimeToWait: Int

	//var stoppedState  : WaiterState
	var stoppedAction: WaiterState
	var stoppedTable: Int

	var stoppedClientId: Int
	// mappa con in chiave id del tavolo e value id del cliente
	var clientIdByTableId = hashMapOf<Int, Int>()
	var orderedProductByTable = hashMapOf<Int, Product>()

	var moneyEarned: Float

	var remainingCleanTimeTableMap = hashMapOf<Int, Long>()

	var maxStayTime = 10000L

	var availableTables: Int
	val maxAvailableTables : Int = 2

	var allowedActions = hashMapOf<Int, AllowedAction>()
	var maxCleanTime: Long;
	var pendingOrders = linkedMapOf<Int, HashMap<Int, String>>();

	init {
		// non serve più
		targetTable = 0
		// non serve più
		targetOrder = 0

		waiterState = WaiterState.AT_HOME
		waiterPosX = 0
		waiterPosY = 0

		waitingClient = false
		waitingClientAtExit = false
		clientFound = false
		minTimeToWait = 0

		stoppedAction = WaiterState.NONE
		stoppedTable = 0
		stoppedClientId = 0

		moneyEarned = 0f
		availableTables = 2

		description = ""

		allowedActions.put(1, AllowedAction.NONE);
		allowedActions.put(2, AllowedAction.NONE);

		orderedProductByTable.put(1, Product.NONE);
		orderedProductByTable.put(2, Product.NONE);

		maxCleanTime = 5000L;
		remainingCleanTimeTableMap.put(1, maxCleanTime);
		remainingCleanTimeTableMap.put(2, maxCleanTime);


		insertTableState(1, TableState.CLEAN)
		insertTableState(2, TableState.CLEAN)

		waiterState = WaiterState.AT_HOME

		insertOrderState(1, OrderState.COMPLETE)
		insertOrderState(2, OrderState.COMPLETE)

		insertTableId(1, 0)
		insertTableId(2, 0)

		insertTableConsumation(1, Product.NONE)
		insertTableConsumation(2, Product.NONE)

	}

	private object GetInstance {
		val INSTANCE = CurrentState()
	}

	companion object {
		val instance: CurrentState by lazy { GetInstance.INSTANCE }
	}

	fun setDescriptionWithArguments(descId: String, vararg args: Any) {
		description = Messages.instance.getMessage(descId, *args);
	}

	fun setPlainDescription(descId: String) {
		description = Messages.instance.getMessage(descId);
	}

	fun getConsumationPrice(key: Int): Float {
		return orderedProductByTable[key]!!.price
	}
	
	fun getlastTableOrder(tableId : Int) : String {
		return orderedProductByTable[tableId]!!.value;
	}

	fun insertTableState(key: Int, value: TableState) {
		tableMap[key] = value
	}

	fun insertTableStateFromString(key: Int, value: String) {
		try {
			val tableState = TableState.valueOf(value.toUpperCase());
			tableMap[key] = tableState
		} catch (ise: IllegalStateException) {
			tableMap[key] = TableState.UNKNOWN
		}
	}

	fun getFirstDirtyTable(): Int {
		tableMap.forEach { (key, value) ->
			//println("Util | Checking table[$key] => $value");
			if (value == TableState.DIRTY) {
				return key
			};
		}
		return 0
	}
	
	fun isTableDirty(key : Int) : Boolean {
		return tableMap[key] == TableState.DIRTY;
	}

	fun decAvailableTables() {
		availableTables--
	}

	fun incAvailableTables() {
		availableTables++
	}

	fun getTablesAvailable(): Int {
		return availableTables
	}

	fun getAvailableTable(): Int {
		var i: Int = 0;
		enumValues<TableState>().forEach {
			if (it == TableState.CLEAN) {
				i++
			}
		}
		return i
	}

	fun getTableState(key: Int): TableState {
		return tableMap[key]!!
	}

	fun insertOrderState(key: Int, value: OrderState) {
		orderMap[key] = value
	}

	fun addPendingOrder(orderName: String, table: Int, clientID: Int) {
		val map: HashMap<Int, String> = hashMapOf<Int, String>()
		map.put(table, orderName)
		pendingOrders.put(clientID, map);
	}

	fun removePendingOrder() {
		if (pendingOrders.size > 0) {
			pendingOrders.remove(getFirstPendingOrder())
		}
	}

	fun getFirstPendingOrder(): Int {
		var counter: Int = 0
		var tempKey: Int = 0
		pendingOrders.forEach { (key) ->
			if (counter == 0) {
				tempKey = key
				counter++
			}
		}
		return tempKey;
	}

	fun getLastPendingOrder(): Int {
		var tempKey: Int = 0
		pendingOrders.forEach { (key) ->
			tempKey = key
		}
		return tempKey;
	}

	fun updateAllowedAction(key: Int, value: AllowedAction) {
		allowedActions[key] = value
	}

	fun getOrderState(key: Int): OrderState {
		return orderMap[key]!!
	}

	fun insertTableId(key: Int, value: Int) {
		clientIdByTableId[key] = value
	}

	fun insertTableConsumation(key: Int, value: Product) {
		orderedProductByTable[key] = value
	}

	fun insertTableConsumationFromString(key: Int, value: String) {
		try {
			val tableState = Product.valueOf(value.toUpperCase());
			println("Util | inserting tableState => $tableState");
			orderedProductByTable[key] = tableState
		} catch (ise: IllegalStateException) {
			orderedProductByTable[key] = Product.UNKNOWN
		}
	}

	fun getClientId(tableId: Int): Int {
		return clientIdByTableId[tableId]!!
	}

	fun insertRemainingCleanTimeForTable(key: Int, value: Long) {
		remainingCleanTimeTableMap[key] = value
	}

	fun getLowerTimeDirtyTable(): Int {
		var index: Int = 0
		if (!remainingCleanTimeTableMap.isEmpty()) {
			var i: Int = 0
			var minValue: Long = maxStayTime
			remainingCleanTimeTableMap.forEach { (key, value) ->
				if (tableMap[key] == TableState.DIRTY) {
					if (i == 0 || value < minValue) {
						index = key
						minValue = value;
						i++
					}
				}
			}
		}
		return index
	}

	fun setMaxStayTimeValue(value: Long) {
		this.maxStayTime = value
	}

	fun resetAllRemainingCleanTime() {
		remainingCleanTimeTableMap.forEach { (key) ->
			remainingCleanTimeTableMap[key] = maxCleanTime;
		}
	}

	fun resetRemainingCleanTime(key: Int) {
		remainingCleanTimeTableMap[key] = maxCleanTime
	}

	fun setRemainingCleanTime(key: Int, value: Long) {
		this.remainingCleanTimeTableMap[key] = value
	}

	fun getRemainingCleanTime(key: Int): Long {
		return remainingCleanTimeTableMap[key]!!;
	}

	fun setMaxRemainingCleanTime(value: Long) {
		this.maxCleanTime = value
		resetAllRemainingCleanTime();
	}

	fun isWaitingClient(): Boolean {
		return waitingClient
	}

	fun commuteWaitingClient() {
		waitingClient = !waitingClient
	}
	
	fun addMoney(value : Float){
		moneyEarned += value
	}

}


fun main() {

	// esempi
	var ct: CheckTemperature = CheckTemperature.CHECK_POSITIVE

	var currState = CurrentState.instance

	currState.insertTableStateFromString(1, "Clean");
	currState.insertTableStateFromString(1, "Dirty");


//	currState.insertTableState(1, TableState.CLEAN)
//	currState.insertTableState(2, TableState.CLEAN)

	currState.waiterState = WaiterState.AT_HOME

	currState.insertOrderState(1, OrderState.READY)
	currState.insertOrderState(2, OrderState.PREPARE)
	currState.updateAllowedAction(1, AllowedAction.READY_FOR_ORDER)
	currState.updateAllowedAction(2, AllowedAction.READY_FOR_ORDER)
//		currState.description = "hello.world"
	var arg: Any
	arg = 1
	currState.setDescriptionWithArguments("moving.table", arg)

	val mapper = jacksonObjectMapper()
//	mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

//	var sbt : SmartBellStatus
	
	var jsonString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(currState);
	
	println("json value of currentState: $jsonString")
	val currentState : CurrentState = mapper.readValue<CurrentState>("{\"tableMap\":{\"1\":\"Clean\",\"2\":\"Clean\"},\"orderMap\":{\"1\":\"Completed\",\"2\":\"Completed\"},\"targetTable\":0,\"targetOrder\":0,\"waiterState\":\"At home\",\"waiterPosX\":0,\"waiterPosY\":0,\"description\":\"Waiter at Home waiting next task\",\"waitingClient\":false,\"waitingClientAtExit\":false,\"clientFound\":false,\"minTimeToWait\":0,\"stoppedAction\":\"None\",\"stoppedTable\":0,\"stoppedClientId\":0,\"clientIdByTableId\":{\"1\":0,\"2\":0},\"orderedProductByTable\":{\"1\":\"Nothing\",\"2\":\"Nothing\"},\"moneyEarned\":0.0,\"remainingCleanTimeTableMap\":{\"1\":10000,\"2\":10000},\"maxStayTime\":10000,\"availableTables\":2,\"maxAvailableTables\":2,\"allowedActions\":{\"1\":5,\"2\":4},\"maxCleanTime\":10000,\"pendingOrders\":{},\"firstDirtyTable\":0,\"tablesAvailable\":2,\"availableTable\":1,\"firstPendingOrder\":0,\"lastPendingOrder\":0,\"lowerTimeDirtyTable\":0}");

//	var jsonString = JSONUtils.toJSON(currState)


//	var id = ct.id
//	var text = ct.text
//	println("check is: " + ct.id)
//
//	var res = 1 % 2;
//	println(res);
//	res = 2 % 2;
//	println(res);
}
