package domain

import com.fasterxml.jackson.module.kotlin.*
import com.fasterxml.jackson.annotation.JsonValue

enum class WaiterState(val id: Int, @get:JsonValue val value: String) {
	AT_HOME(1, "At home"),
	MOVING_TO_HOME(2, "Moving to home"),
	MOVING_TO_ENTRANCE(3, "Moving to entrance"),
	DEPLOY_CLIENT(4, "Deploy client"),
	CLIENT_DEPLOYED(5, "Client deployed"),
	READY_FOR_ORDER(6, "Ready for order"),
	GET_ORDER(7, "Get order"),
	TRANSMITTING_ORDER(8, "Transmitting Order"),
	MOVING_TO_BARMAN(9, "Moving to barman"),
	DELIVER_ORDER(10, "Deliver order"),
	SERVING(11, "Serving"),
	COLLECTING(12, "Collecting"),
	PAYMENT_COLLECTED(13, "Payment Collected"),
	TAKE_OUT(14, "Take out"),
	VERIFY_ORDER(15, "Verify order"),
	GOING_CLEAN(16, "Going clean"),
	CLEANING(17, "Cleaning"),
	CLEANED(18, "Cleaned"),
	STOP_ACTION(19, "Stop action"),
	STOP_MOVEMENT(20, "Stop movement"),
	RESTORE_ACTION(21, "Restore action"),
	MOVING_TO_EXIT(22, "Conveying Client to Exit"),
	AT_ENTRANCE(23, "Welcome Client"),
	AT_EXIT(24, "At exit"),
	ERROR(25, "An Error Occurred"),
	WAIT_FOR_MAP(26, "Wait for map"),
	NONE(27, "None");

	override fun toString(): String {
		return "${super.toString()}($id,$value)"
	}
}