package domain

import com.fasterxml.jackson.module.kotlin.*
import com.fasterxml.jackson.annotation.JsonValue

enum class Product(val id: Int, @get:JsonValue val value: String, val price: Float) {
	NONE(1, "Nothing", 0f),
	TEA(2, "Tea", 2.0f),
	COFFEE(3, "Coffee", 3.0f),
	COKE(4, "Coke", 4.0f),
	GINTONIC(5, "GinTonic", 5.0f),
	OUT_OF_MENU(5, "Out_of_menu", 7.0f),
	UNKNOWN(6, "Unknown", 6.0f);
}