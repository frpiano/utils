package domain

enum class CheckTemperature(val id: Int, val text: String) {
	CHECK_POSITIVE(1, "Temperature is in an acceptable range"),
	CHECK_NEGATIVE(2, "Temperature is not in an acceptable range"),
	NO_CHECK(3, "Temperature not checked yet");

	override fun toString(): String {
		return "${super.toString()}($id,$text)"
	}

}