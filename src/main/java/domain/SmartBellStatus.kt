package domain

class SmartBellStatus() {
	var checkTemperature: CheckTemperature
	var timeToWait: Int
	var receivedClientId : Int
	var receivedTableId : Int
	var clientWaiting : Boolean
	var smartBellState : SmartBellState

	init {
		checkTemperature = CheckTemperature.NO_CHECK
		timeToWait = 0
		receivedClientId = 0
		receivedTableId = 0
		clientWaiting = false
		smartBellState = SmartBellState.UNKNOWN
	}
	
	fun reset() {
		checkTemperature = CheckTemperature.NO_CHECK
		timeToWait = 0
		receivedClientId = 0
		receivedTableId = 0
	}

	fun decTimeToWait(){
		timeToWait--
	}
	
	fun resetTimeToWait(){
		timeToWait = 0
	}
}