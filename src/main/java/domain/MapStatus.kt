package domain

class MapStatus() {

	var roomMap: String

	init {
		roomMap = ""
	}

	fun reset() {
		roomMap = ""
	}

	fun updateMap(map: String) {
		roomMap = map
		//println("Updated roomMap = $roomMap")
	}
}