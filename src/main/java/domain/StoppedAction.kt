package domain

import com.fasterxml.jackson.module.kotlin.*
import com.fasterxml.jackson.annotation.JsonValue

enum class StoppedAction(val id: Int, @get:JsonValue val value: String) {
	NONE(1, "None"),
	CLEANING(2, "Cleaning"),
	ENTERING(3, "Entering")
}