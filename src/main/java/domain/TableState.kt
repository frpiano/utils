package domain

import com.fasterxml.jackson.module.kotlin.*
import com.fasterxml.jackson.annotation.JsonValue

enum class TableState(val id: Int, @get:JsonValue val value: String) {
	CLEAN(1, "Clean"),
	DIRTY(2, "Dirty"),
	BUSY(3, "Busy"),
	// non dovrebbe mai averlo, inserito per fare controlli di robustezza
	UNKNOWN(4, "Unknown");

	override fun toString(): String {
		return "${super.toString()}($id,$value)"
	}
}