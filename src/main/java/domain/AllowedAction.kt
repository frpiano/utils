package domain

import com.fasterxml.jackson.module.kotlin.*
import com.fasterxml.jackson.annotation.JsonValue
import com.fasterxml.jackson.annotation.JsonCreator
// Serialization hintss
// https://medium.com/@sergio.igwt/kotlin-with-jackson-deserializing-kotlin-sealed-classes-c95f837e9164
enum class AllowedAction(@get:JsonValue val id: Int, val value: String) {
	READY_FOR_ORDER(1, "Ready for order"),
	ORDER(2, "Order"),
	CONSUME_FINISHED(3, "Consume Finished"),
	PAY(4, "Pay"),
	NONE(5, "Nothing");
	
	// risolve il problema di deserializzazione per cui le enum vengono deserializzate usando
	// l'indice di posizione degli elementi dell'enum base ossia partendo da 0 e non da quello che noi abbiamo
	// inserito come id
	companion object {
        @JsonCreator
        @JvmStatic
        private fun creator(id: Int): AllowedAction? {
            return AllowedAction.values().firstOrNull { it.id == id }
        }
    }
}